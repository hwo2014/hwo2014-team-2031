<?php
define('MAX_LINE_LENGTH', 1024 * 1024);

require_once("Dijkstra.php");
require_once 'fonctions.php';

class BasicBot {
    protected $sock, $debug;

    private $raceData,
        $nbPieces,
        $nbLanes,
        $currentPieceIndex,
        $currentPieceLength,
        $currentLaneRadius,
        $currentLap,
        $currentTick,
        $myCarPosition,
        $myCarName,
        $myCarData,
        $myCarStats,
        $myCarThrottle,
        $raceGraph,
        $bestPath,
        $burn,
        $waitSwitchingLane,
        $switchSide,
        $gameStart,
        $turboAvaible,
        $turboData,
        $targets,
        $targetIndex,
        $coeffDeceleration,
        $coeffBurn,
        $coeffAirFriction,
        $vitesseRegulateHack,
        $vitesseStable,
        $learningCoeffIsFinish,
        $bendsData;



    //-----------------------------------------------------------------------------------------------------//
    //                                                 CONSTRUCTEUR                                        //
    //-----------------------------------------------------------------------------------------------------//

    function __construct($host, $port, $botname, $botkey, $debug = FALSE)
    {
        $this->debug = $debug;  //pour débugger
        $this->myCarName = $botname; //the teamName
        $this->myCarThrottle = 1; //a fond les ballons ;)
        $this->waitSwitchingLane = false;
        $this->turboAvaible = false;
        $this->bendsData = array();
        $this->currentPieceIndex = -1;
        $this->currentLap = -1;
        $this->currentTick = -1;
        $this->burn = false;
        $this->gameStart = false;
        $this->vitesseStable = false;
        $this->learningCoeffIsFinish = false;
        $this->vitesseRegulateHack = 0.000000000001;
        $this->myCarStats = array( "pieceLength"        => array(),     //distance du tronçon
            "inPieceDistance"    => array(),     //distance de la voiture à l'intérieur du tronçon
            "pieceIndex"         => array(),     //index de la piece
            "throttle"           => array(),     //les gazs
            "speed"              => array(),     //vitesse réelle
            "acceleration"       => array(),     //accélération
            "nextAcceleration"   => array(),     //anticipation de l'accélération
            "erreurAcceleration" => array(),     //erreur accélération virtuelle
            "laneRadius"         => array(),     //radius de la voie dans un virage
            "brakingDistance"    => array(),     //distance de freinage
            "centrifugalForce"   => array(),     //force Centrifuge
            "drift"              => array());    //drift de la voiture

        $this->initStats();

        //connection
        $this->connect($host, $port, $botkey);

        $this->write_msg('join', array(
            'name' => $botname,
            'key' => $botkey
        ));

        /*
        $this->write_msg('joinRace', array( 'botId' => array('name' => $botname,
                                                            'key' => $botkey),
                                            'trackName' => 'germany',
                                            'carCount' => 1));

        */

    }

    function __destruct() {
        if (isset($this->sock)) {
            socket_close($this->sock);
        }
    }





    //-----------------------------------------------------------------------------------------------------//
    //                                                 CONNECTION                                          //
    //-----------------------------------------------------------------------------------------------------//

    protected function connect($host, $port, $botkey) {
        $this->sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if ($this->sock === FALSE) {
            throw new Exception('socket: ' . socket_strerror(socket_last_error()));
        }
        if (!socket_connect($this->sock, $host, $port)) {
            throw new Exception($host . ': ' . $this->sockerror());
        }
    }

    protected function read_msg() {
        $line = socket_read($this->sock, MAX_LINE_LENGTH, PHP_NORMAL_READ);
        if ($line === FALSE) {
            $this->debug('** ' . $this->sockerror());
        } else {
            $this->debug('<= ' . rtrim($line));
        }
        return json_decode($line, TRUE);
    }


    protected function write_msg($msgtype, $data, $tick = -1) {
        $tab = array('msgType' => $msgtype, 'data' => $data);
        if($tick != -1) $tab['gameTick'] = $tick;

        $str = json_encode($tab) . "\n";
        $this->debug('=> ' . rtrim($str));
        if (socket_write($this->sock, $str) === FALSE) {
            throw new Exception('write: ' . $this->sockerror());
        }
    }

    private function saveData()
    {
        if($this->debug)
        {
            //création du fichier csv

            //entete CSV
            $i=0;
            $strCSV="";
            foreach($this->myCarStats as $name => $tab)
            {
                $strCSV .= $name.";";
                $i ++;
                if($i == (count($this->myCarStats)))
                {
                    $strCSV .= "\n";
                }
            }

            //data CSV
            $nbValue = count($this->myCarStats["pieceIndex"]);
            for($j = 0; $j < $nbValue; $j++)
            {
                $i=0;
                foreach($this->myCarStats as $tab)
                {
                    $strCSV .= $tab[$j].";";
                    $i ++;
                    if($i == (count($this->myCarStats)))
                    {
                        $strCSV .= "\n";
                    }
                }
            }

            //ecriture CSV
            $csv = fopen('graphique/json/data.csv', 'w+');
            fwrite($csv, $strCSV);
            fclose($csv);


            //Creation du JSON
            $jsonData = array();
            foreach($this->myCarStats as $name => $tab)
            {
                $jsonData[] = array("name" => $name, "data" => $tab);

            }

            //Ecriture JSON
            $strJson = json_encode($jsonData);
            $fp = fopen('graphique/json/data.json', 'w+');
            fwrite($fp, $strJson);
            fclose($fp);

        }
    }

    protected function sockerror() {
        return socket_strerror(socket_last_error($this->sock));
    }

    protected function debug($msg) {
        if ($this->debug) {
            echo $msg, "\n";
        }
    }




    //-----------------------------------------------------------------------------------------------------//
    //                                              RUN AND WIN !                                          //
    //-----------------------------------------------------------------------------------------------------//

    //receptionne l'ensemble des message envoyer par le serveur et  répond si nécessaire
    public function run()
    {
        while (!is_null($msg = $this->read_msg())) {
            switch ($msg['msgType']) {

                //initialisation des paramètre de la course
                case 'gameInit'     :
                    $this->initParameters($msg);
                    break;

                //début de la course
                case 'gameStart'    :
                    $this->gameStart = true;
                    $this->burn = false;
                    $this->go($msg);
                    break;

                //conduit la voiture
                case 'carPositions' :
                    $this->go($msg);
                    break;

                case 'turboAvailable' : $this->turboAvaible = true;
                    $this->turboData = $msg['data'];
                    break;



                case 'crash'        :   $this->debugMsg($msg, "Crash");
                    break;


                case 'spawn'        :   $this->burn = false;
                                        break;


                //afficher les résultats de la course
                case 'gameEnd'      :
                    $this->saveData();
                    $this->printTrackResults($msg);
                    break;

                //ne rien faire
                case 'join'         :
                case 'yourCar'      :

                case 'lapFinished'  :
                case 'dnf'          :
                    //envoyer un ping
                default             :   $this->write_msg('ping', null);

            }
        }
    }




    //-----------------------------------------------------------------------------------------------------//
    //                                              INITIALISATION                                         //
    //-----------------------------------------------------------------------------------------------------//

    //initialisation des paramètres de la course et du bot
    private function initParameters($msg)
    {
        $this->raceData = $msg['data']['race'];
        $this->nbPieces = count($this->raceData["track"]["pieces"]);
        $this->nbLanes = count($this->raceData["track"]["lanes"]);

        //trouve l'index de la voiture en fonction du nom
        //ATTENTION à vérifier avec plusieurs concurrent si l'index ne change pendant la course
        foreach($this->raceData['cars'] as $i => $car)
        {
            if($this->myCarName == $car["id"]["name"])
            {
                $this->myCarPosition = $i;
            }
        }

        //construit le graph du circuit nécessaire pour trouver le chemin le plus court
        $this->buildRaceGraph();
        $this->bestPath = $this->findTheBestPath();


        $this->initTarget();

        $this->initCoeff();
    }





    //-----------------------------------------------------------------------------------------------------//
    //                                           PEDAL ON THE METAL !!!!                                   //
    //-----------------------------------------------------------------------------------------------------//
    private function go($msg)
    {

        if($msg["msgType"] == 'carPositions')
        {
            //met à jour les paramètres
            $this->updateParameters($msg);

            //met à jour les statistiques de la voiture et de la course
            $this->updateStats($msg);
        }

        if($msg["msgType"] == 'gameStart'
            || isset($msg["gameTick"]))
        {
            //pilote la voiture;
            $this->driveMyCar();
        }

        //met à jour les infos sur la prochaine acceleration et la puissance envoyé
        $this->updateThrottle($msg);
    }


    //envoie le message de vitesse et de changement de direction
    private function driveMyCar()
    {
        $this->learnTrack();

        if($this->learningCoeffIsFinish)
        {
            //change de voie si nécessaire
            if(!$this->computeSwitch())//un seul message à la fois
            {
                //envoie le turbo si c'est le bon moment
                if(!$this->turbo())
                {
                    //calcule la vitesse idéale et envoie le message de vitesse
                    $this->computeThrottle();
                }
            }
        }
    }


    private function learnTrack()
    {
        if(!$this->learningCoeffIsFinish)
        {
            switch($this->currentTick)
            {
                //a fond
                case 0:
                case 1:
                case 2:
                case 3:
                    $this->write_msg('throttle', 1, $this->currentTick);
                break;

                //decelere
                case 4:
                    $this->write_msg('throttle', 0, $this->currentTick);
                    break;

                //analyse
                case 5:
                    $this->coeffBurn = $this->myCarStats["acceleration"][3];
                    $this->coeffAirFriction = $this->myCarStats["acceleration"][4]/$this->myCarStats["acceleration"][3];
                    $this->coeffDeceleration = abs($this->myCarStats["acceleration"][5] / $this->myCarStats["speed"][4]);
                    $this->learningCoeffIsFinish = true;
                    break;
            }
        }
    }

    private function initCoeff()
    {
        $this->coeffDeceleration = 0.02;
        $this->coeffBurn = 0.2;
        $this->coeffAirFriction = 0.98;
    }


    //envoie le turbo si c'est ok
    private function turbo()
    {
        return false;

        if($this->turboAvaible)
        {
            // utilisez $this->turboData; pour calculer si le boost est possible
            /*
             {"msgType": "turboAvailable", "data": {
                  "turboDurationMilliseconds": 500.0,
                  "turboDurationTicks": 30,
                  "turboFactor": 3.0
             }
            */

            //envoie la poussée de l'accelerateur au serveur
            if($this->currentPieceIndex == 34 && $this->myCarData['piecePosition']['inPieceDistance'] > 40)
            {
                $this->write_msg('turbo', 'Gadget o Boost !!!', $this->currentTick);
                $this->turboAvaible = false;
                return true;
            }
        }
        return false;
    }


    //calcule la poussée de l'accelerateur idéale en fonction de différents paramètres
    private function computeThrottle()
    {
        //recuperer la prochaine cible
        if($this->targetIsPassed())
        {
            $this->targetIndex = $this->getFollowTargetIndex($this->targetIndex);
        }

        //adapter ma vitesse en fonction de la prochaine cible
        $throttle = $this->controlThrottle();

        //regulateur
        $this->myCarThrottle = max(0,min(1.0,$throttle));

        //envoie la poussée de l'accelerateur au serveur
        $this->write_msg('throttle', $this->myCarThrottle, $this->currentTick);
    }


    //envoie le message de changement de voie si nécessaire
    private function computeSwitch()
    {
        $followPieceIndex = $this->getFollowPieceIndex($this->currentPieceIndex);

        //si le prochain tronçon est un switch
        if(isset($this->raceData['track']['pieces'][$this->currentPieceIndex]['switch'])
            || isset($this->raceData['track']['pieces'][$followPieceIndex]['switch']))
        {
            //essaye de se rapprocher du la voie idéale.
            $bestLane = $this->bestPath[$this->getFollowPieceIndex($followPieceIndex)];
            $currentStartLane = $this->myCarData['piecePosition']['lane']['startLaneIndex'];
            $currentEndLane = $this->myCarData['piecePosition']['lane']['endLaneIndex'];

            //reset après changement de voie
            if($this->waitSwitchingLane && $currentStartLane != $currentEndLane)
            {
                $this->waitSwitchingLane = false;
            }
            //changer de voie
            else if(!$this->waitSwitchingLane && $bestLane !=  $currentEndLane)
            {
                $this->waitSwitchingLane = true;
                $this->switchSide = ($bestLane < $currentStartLane)? "Left" : "Right";
                $this->write_msg('switchLane', $this->switchSide, $this->currentTick);
                return true;
            }
        }
        return false;
    }

    //Maj des parametres du bot
    private function updateParameters($msg)
    {
        //ATTENTION à vérifier avec plusieurs concurrent si l'index ne change pendant la course
        foreach($msg['data'] as $i => $car)
        {
            if($this->myCarName == $car["id"]["name"])
            {
                $this->myCarPosition = $i;
            }
        }

        $this->currentTick = (isset($msg['gameTick']))? $msg['gameTick']: 0;
        $this->myCarData = $msg['data'][$this->myCarPosition];
        $startLaneIndex = $this->myCarData['piecePosition']['lane']['startLaneIndex'];
        $endLaneIndex = $this->myCarData['piecePosition']['lane']['endLaneIndex'];

        //change l'index de la pièce courante
        $pieceIndex = $this->myCarData['piecePosition']['pieceIndex'];


        if($this->currentPieceIndex != $pieceIndex)
        {
            $this->currentPieceIndex = $pieceIndex;//maj l'index

            //récupère la longueur de la pièce
            $this->currentPieceLength = $this->getRealPieceLength($pieceIndex,$startLaneIndex, $endLaneIndex);
        }

        /*
        if($this->currentLap == 1)
        {
            printLn("lane radius : ".$this->currentLaneRadius);
            printLn("currentPieceIndex : ".$this->currentPieceIndex);
            printLn("pieceIndex : ".$pieceIndex);
            printLn("myCarPosition : ".$this->myCarPosition);
            printLn("test : ".($this->currentPieceIndex != $pieceIndex));
            printLn("coeffBurn : ".$this->coeffBurn);
            printLn("coeffAirFriction : ".$this->coeffAirFriction);
            printLn("coeffDeceleration : ".$this->coeffDeceleration);
            print_r($this->myCarData);
            $this->saveData();
            die;
        }
*/
        if($this->currentLap != $this->myCarData['piecePosition']['lap'])
        {
            $this->currentLap = $this->myCarData['piecePosition']['lap'];
        }
    }


    //-----------------------------------------------------------------------------------------------------//
    //                                             TARGET                                                  //
    //-----------------------------------------------------------------------------------------------------//

    //calcule la distance jusqu'à une piece donnée
    private function getDistanceOfPiece($TargetPieceIndex)
    {
        //calcule la distance jusqu'à la prochaine piece
        $distanceOfTarget = $this->currentPieceLength - $this->myCarData['piecePosition']['inPieceDistance'];

        $pieceIndex = $this->getFollowPieceIndex($this->currentPieceIndex);
        $startLaneIndex = $this->myCarData['piecePosition']['lane']['endLaneIndex'];

        //tant qu'on est pas à la piece objectif, ajoute la distance avec la prochain piece
        while($pieceIndex != $TargetPieceIndex)
        {
            //si le prochain est un échangeur alors on prend le meilleur Path
            $endLaneIndex = $this->bestPath[$pieceIndex];

            $distanceOfTarget += $this->getRealPieceLength($pieceIndex,$startLaneIndex, $endLaneIndex);

            //si on change de voie, start devient end
            if($startLaneIndex != $endLaneIndex && isset($this->raceData['track']['pieces'][$pieceIndex]['switch']))
            {
                $startLaneIndex = $endLaneIndex;
            }

            $pieceIndex = $this->getFollowPieceIndex($pieceIndex);//on passe à la piece suivante
        }
        return $distanceOfTarget;
    }



    private function initTarget()
    {

        $this->targetIndex = 0;
        $this->targets = array(
            array("pieceIndex"=>2,"inPieceDistance"=>0,"maxSpeed"=>6.3,"regular"=>false),
            array("pieceIndex"=>38,"inPieceDistance"=>40,"maxSpeed"=>6.3,"regular"=>true));
        /*
            array("pieceIndex"=>4,"inPieceDistance"=>0,"maxSpeed"=>6.555,"regular"=>false),
            //array("pieceIndex"=>9,"inPieceDistance"=>0,"maxSpeed"=>6.555,"regular"=>true),
            array("pieceIndex"=>7,"inPieceDistance"=>40,"maxSpeed"=>6.555,"regular"=>true),
            array("pieceIndex"=>14,"inPieceDistance"=>0,"maxSpeed"=>6.565,"regular"=>false),
            array("pieceIndex"=>17,"inPieceDistance"=>40,"maxSpeed"=>6.565,"regular"=>true),
            array("pieceIndex"=>19,"inPieceDistance"=>0,"maxSpeed"=>6.46,"regular"=>false),
            array("pieceIndex"=>22,"inPieceDistance"=>50,"maxSpeed"=>6.46,"regular"=>true),
            array("pieceIndex"=>26,"inPieceDistance"=>0,"maxSpeed"=>6.15,"regular"=>false),
            array("pieceIndex"=>29,"inPieceDistance"=>0,"maxSpeed"=>6.15,"regular"=>false),
            array("pieceIndex"=>31,"inPieceDistance"=>0,"maxSpeed"=>6.3,"regular"=>false),
            array("pieceIndex"=>34,"inPieceDistance"=>40,"maxSpeed"=>6.3,"regular"=>true));*/


        /*
        foreach($this->raceData['track']['pieces'] as $pieceIndex => $piece)
        {
            $speedTarget = 0;
            //si la piece suivante est un virage
            $this->targets[] = array("vitesse" => $speedTarget,"indexPiece" => $pieceIndex, "inPosition" => 0);
        }
        */
    }


    //récupère l 'index suivant du target
    private function getFollowTargetIndex($targetIndex)
    {
        $followTargetIndex = ($targetIndex == count($this->targets) - 1 )? 0 : $targetIndex + 1;
        return $followTargetIndex;
    }

    //récupère l 'index précedent du target
    private function getPreviousTargetIndex($targetIndex)
    {
        $previousTargetIndex = ($targetIndex == 0 )? count($this->targets) - 1 : $targetIndex - 1;
        return $previousTargetIndex;
    }


    private function targetIsPassed()
    {
        //retourne vrai, si la voiture vient de passer la cible courante alors on passe à la prochaine
        $currentInPieceDistance = $this->myCarData["piecePosition"]["inPieceDistance"];

        return ($this->currentPieceIndex == $this->targets[$this->targetIndex]["pieceIndex"]
            && $currentInPieceDistance >= $this->targets[$this->targetIndex]["inPieceDistance"]);
    }


    //-----------------------------------------------------------------------------------------------------//
    //                                              PIECES FUNCTIONS                                       //
    //-----------------------------------------------------------------------------------------------------//


    //récupère le prochain index de piece
    private function getFollowPieceIndex($pieceIndex)
    {
        $followPieceIndex = ($pieceIndex == $this->nbPieces - 1 )? 0 : $pieceIndex + 1;
        return $followPieceIndex;
    }

    //récupère l 'index précedent de piece
    private function getPreviousPieceIndex($pieceIndex)
    {
        $previousPieceIndex = ($pieceIndex == 0 )? $this->nbPieces - 1 : $pieceIndex - 1;
        return $previousPieceIndex;
    }

    private function getLaneRadius($radius,$angle,$distanceFromCenter)
    {
        $distance = ($angle > 0)? - $distanceFromCenter : $distanceFromCenter ;
        $newRadius = $radius + $distance;
        return $newRadius;
    }

    private function getPieceLength($pieceIndex,$laneIndex)
    {
        if(! isset($this->raceData['track']['pieces'][$pieceIndex]['length'])) //virage
        {
            $pi = 3.1415926535898;
            $angle = $this->raceData['track']['pieces'][$pieceIndex]['angle'];
            $radius = $this->raceData['track']['pieces'][$pieceIndex]['radius'];
            $distanceFromCenter = $this->raceData['track']['lanes'][$laneIndex]['distanceFromCenter'];

            $this->currentLaneRadius = $this->getLaneRadius($radius,$angle,$distanceFromCenter);
            $pieceLength = ($pi * abs($angle) * $this->currentLaneRadius) / 180;
        }
        else    //ligne droite
        {
            $pieceLength = $this->raceData['track']['pieces'][$pieceIndex]['length'];
            $this->currentLaneRadius = 0;
        }
        return $pieceLength;
    }


    private function getSwitchLength($pieceIndex,$laneIndex, $followLaneIndex)
    {
        //calcul de l'hypothènuse
        if(isset($this->raceData['track']['pieces'][$pieceIndex]['radius']))
        {
            $this->currentLaneRadius = 0;
            $angle = abs($this->raceData['track']['pieces'][$pieceIndex]['angle']);
            $radius = $this->raceData['track']['pieces'][$pieceIndex]['radius'];
            $ab = $radius + $this->raceData['track']['lanes'][$laneIndex]['distanceFromCenter'];
            $ac = $radius + $this->raceData['track']['lanes'][$followLaneIndex]['distanceFromCenter'];
            $bc = sqrt( pow($ab,2) +  pow($ac,2) - (2 * $ac * $ab * cos(deg2rad($angle))));
            if($bc > 80 && $bc < 90)
            {
                $bc = 81.0546522063746;
            }
        }
        else
        {
            $angle = 90;
            $ab = abs($this->raceData['track']['lanes'][$laneIndex]['distanceFromCenter'] - $this->raceData['track']['lanes'][$followLaneIndex]['distanceFromCenter']);
            $ac = $this->raceData['track']['pieces'][$pieceIndex]['length'];
            $this->currentLaneRadius = 0;
            $bc = sqrt( pow($ab,2) +  pow($ac,2) - (2 * $ac * $ab * cos(deg2rad($angle))));
            if($ac = 100)
            {
                $bc = 102.059949828356;
            }
        }
        return $bc;
    }

    private function getRealPieceLength($pieceIndex,$startLaneIndex, $endLaneIndex)
    {
        if($startLaneIndex != $endLaneIndex && isset($this->raceData['track']['pieces'][$pieceIndex]['switch']))// && isset($this->raceData['track']['pieces'][$pieceIndex]['radius'])) //pour un switch dans un virage
        {
            return $this->getSwitchLength($pieceIndex,$startLaneIndex, $endLaneIndex);
        }
        else //pour un piece normal
        {
            return $this->getPieceLength($pieceIndex,$startLaneIndex);
        }
    }


    private function getBuildingPieceLength($pieceIndex,$startLaneIndex, $endLaneIndex)
    {
        return $this->getRealPieceLength($pieceIndex,$startLaneIndex, $endLaneIndex);
    }

    //-----------------------------------------------------------------------------------------------------//
    //                                              RACE FUNCTIONS                                         //
    //-----------------------------------------------------------------------------------------------------//


    //construit le graph de la course
    private function buildRaceGraph()
    {
        //dans un premier temps on va essayer de trouver le chemin le plus court avec l'algo de Dijkstra
        $this->raceGraph = new Graph();

        //pour chaque couple troncon/voie calculer la distance
        foreach($this->raceData['track']['pieces'] as $pieceIndex => $piece)
        {
            foreach($this->raceData['track']['lanes'] as $laneIndex => $lane)
            {
                //construire un tableau des voie accesible
                $accessibleLane = array($laneIndex);

                //si on peut changer de voie, ajouter les voie dispo
                if(isset($piece['switch']))
                {
                    if($laneIndex > 0 )
                    {
                        $accessibleLane[] = $laneIndex - 1;
                    }

                    if($laneIndex < $this->nbLanes - 1)
                    {
                        $accessibleLane[] = $laneIndex + 1;
                    }
                }

                foreach($accessibleLane as $followLaneIndex)
                {
                    $followPieceIndex = $this->getFollowPieceIndex($pieceIndex);

                    $distance = $this->getBuildingPieceLength($pieceIndex,$laneIndex, $followLaneIndex);

                    $from = indexName($pieceIndex,$laneIndex);
                    $to = indexName($followPieceIndex,$followLaneIndex);
                    $this->raceGraph->addedge($from, $to, $distance);

                }
            }
        }
    }

    //teste pour toutes les position de départ et d'arrivée,
    //quel chemin est le plus court
    private function findTheBestPath()
    {
        $bestPath = array();
        $bestTotalDistance = INF;

        for($indexStartLane = 0; $indexStartLane < $this->nbLanes; $indexStartLane++)
        {
            for($indexEndLane = 0; $indexEndLane < $this->nbLanes; $indexEndLane++)
            {
                list($distance, $predecessors) = $this->raceGraph->paths_from(indexName(0,$indexStartLane));
                $path = $this->raceGraph->paths_to($predecessors, indexName($this->nbPieces - 1,$indexEndLane));

                //si la distance parcourue est la meilleure alors on sauvegarde le parcours idéal
                if($distance[$path[count($path) - 1]] < $bestTotalDistance)
                {
                    $bestTotalDistance = $distance[$path[count($path) - 1]];
                    $bestPath = $path;
                }
            }
        }

        //formate le tableau pour avoir indexPiece => indexLane
        foreach($bestPath as $indexPiece => $value)
        {
            list(,$indexLane) = explode("_",$value);
            list(,$indexLane) = explode("_",$value);
            $bestPath[$indexPiece] = $indexLane;
        }

        return $bestPath;
    }

    //-----------------------------------------------------------------------------------------------------//
    //                                  Control Throttle                                                   //
    //-----------------------------------------------------------------------------------------------------//


    //trouve la distance à laquelle il faudra freiner ou accélerer avant une vitesse cible
    private function controlThrottle()
    {
        $tick = $this->currentTick;
        $currentTarget =  $this->targets[$this->targetIndex];

        //on se place à t + 1
        //car le serveur à un coup de retard sur la réponse à l'accélérateur
        $acc1 = $this->computeNextAccelerationByTick($tick);
        $v1 =  $this->myCarStats['speed'][$tick] + $acc1;

        $acc0 = $this->myCarStats['acceleration'][$tick];
        $v0 =  $this->myCarStats['speed'][$tick];
        $p0 = $this->myCarStats['throttle'][$tick];

        $limitDistanceToDecelerate = $this->getLimitDistanceToDecelerateBeforeTarget($v0,$acc0,$p0,0,$currentTarget["maxSpeed"]);
        $distanceToPieceTarget = $this->getDistanceOfPiece($currentTarget["pieceIndex"]) + $currentTarget["inPieceDistance"];

        //si la voiture a atteint le point limite de freinage et que ma vitesse
        //besoin de ralentir
        if($v0 > $currentTarget["maxSpeed"] && $limitDistanceToDecelerate > ($distanceToPieceTarget - $v1))
        {
            $bestThrottle = 0;
        }
        //si la prochaine cible a la même vitesse une cible que la cible precedente
        else if($p0 != 0 && $currentTarget["regular"])
        {
            $bestThrottle = $this->regulateThrottle($v0,$acc0,$p0,$currentTarget["maxSpeed"]);
            $v1 = $this->myCarStats['speed'][$tick] + $this->computeNextAcceleration($v0,$acc0,$p0,$bestThrottle);
            if($v1 > $currentTarget["maxSpeed"])//si la prochaine vitesse dépasse vTarget alors je garde ma vitesse
            {
                $bestThrottle = $this->regulateThrottle($v0,$acc0,$p0,$v0);
            }
        }
        else
        {
            $bestThrottle = 1.0;//a fond les manettes !
        }

        $bestThrottle = $this->regulateThrottle($v0,$acc0,$p0,6);

        return $bestThrottle;
    }

    private function progressThrottle($v0,$acc0,$p0,$v1)
    {


    }

    private function regulateThrottle($v0,$acc0,$p0,$v1)
    {
        if($v0 == $v1)
        {
            //..un coup on diminiue, on coup on augmente
            if($this->vitesseStable)
            {
                $v1 += $this->vitesseRegulateHack;
                $this->vitesseStable = false;
            }
            else
            {
                $v1 -= $this->vitesseRegulateHack;
                $this->vitesseStable = true;
            }
        }
        else
        {
            $this->vitesseStable = false;
        }

        $p1 = $p0;
        if($p0 == 0)//accelere à partir de zero progressivement
        {
            $accFull = $acc0 * $this->coeffAirFriction;
            $p1 = ($v1 - $v0 - $accFull)/$this->coeffBurn;
        }
        else if($p0 != 0)//accelere ou decelere progressivement
        {
            $accFull = $acc0 * $this->coeffAirFriction;
            $accEmpty = - $v0 * $this->coeffDeceleration;
            $step = $accFull - $accEmpty;
            if($step != 0) $p1 = ( $p0 * ($v1 - $v0 - $accEmpty) ) /  $step;
        }

        return $p1;
    }


    private function computetNextSpeed($tick)
    {
        $acc1 = $this->computeNextAccelerationByTick($tick);
        $v1 =  $this->myCarStats['speed'][$tick] + $acc1;
        return $v1;
    }


    //retourne l'acceleration potentielle au prochain tick de l'horloge serveur
    private function computeNextAccelerationByTick($tick)
    {
        $v0 = $this->myCarStats['speed'][$tick];
        $acc0 = $this->myCarStats['acceleration'][$tick];
        $p0 = $this->myCarStats['throttle'][$tick];
        if(!isset($this->myCarStats['throttle'][$tick + 1]))//prochaine accélération virtuelle
        {
            $p1 = $p0;
        }
        else
        {
            $p1 = $this->myCarStats['throttle'][$tick + 1];//prochaine accélration physique
        }

        return $this->computeNextAcceleration($v0,$acc0,$p0,$p1);
    }

    //retourne l'acceleration potentielle au prochain tick de l'horloge serveur
    private function computeNextAcceleration($v0,$acc0,$p0,$p1)
    {
        $acc1 = 0;
        if($this->gameStart)
        {
            if($p1 == 0)
            {
                $acc1 = - $v0 * $this->coeffDeceleration;//relache complétement l'accelerateur
            }
            else if($acc0 == 0 && $p1 > 0)//Démarrage
            {
                $acc1 = $p1 * $this->coeffBurn;
            }
            else if($p0 == $p1 && $p0 != 0)//accelere de façon constante
            {
                $acc1 = $acc0 * $this->coeffAirFriction;
            }
            else if($p0 == 0)//accelere à partir de zero progressivement
            {
                $accFull = $p1 * $this->coeffBurn;
                $acc1 = $acc0 * $this->coeffAirFriction + $accFull;
            }
            else if($p0 != 0)//accelere ou decelere progressivement
            {
                $accFull = $acc0 * $this->coeffAirFriction;
                $accEmpty = - $v0 * $this->coeffDeceleration;
                $step = $accFull - $accEmpty;
                $acc1 = $accEmpty + ($p1/$p0) * $step;
            }
        }

        // -0,079884721 pour entrer dans le virage

        //on régule l'accéleration pour palier au peak de deceleration avant les virages ;)
        //return max(-0.2,min($acc1,0.2));
        return $acc1;
    }

    //trouver la distance limite avant la deceleration pour une vitesse objectif à une vitesse donnée
    private function getLimitDistanceToDecelerateBeforeTarget($v0,$acc0,$p0,$p1,$vTarget)
    {
        if($vTarget == 0) $vTarget = 0.000000000001;
        $distance = 0;
        if($v0 > $vTarget)
        {
            while($v0 > $vTarget && $v0 > 0)
            {
                //calculer
                $acc1 = $this->computeNextAcceleration($v0,$acc0,$p0,$p1);
                $p0 = $p1;
                $acc0 = $acc1;
                $v0 = $v0 + $acc1;
                $distance += $v0;
            }
        }
        return $distance;
    }


    //-----------------------------------------------------------------------------------------------------//
    //                                         COMPUTING                                                   //
    //-----------------------------------------------------------------------------------------------------//

    private  function initStats()
    {
        //initialise les deux premiere valeurs
        foreach($this->myCarStats as $key => $value)
        {
            $this->myCarStats[$key][0] = 0;
            $this->myCarStats[$key][1] = 0;
            $this->myCarStats[$key][2] = 0;
        }
    }

    private function updateThrottle($msg)
    {
        $tick = $this->currentTick;

        //la puissance des gazs au prochain tour
        $this->myCarStats['throttle'][$tick + 1] = $this->myCarThrottle;

        //accélération virtuelle
        $this->myCarStats['nextAcceleration'][$tick + 1] = $this->computeNextAccelerationByTick($tick);

        //erreur calcul accélération virtuelle
        $this->myCarStats['erreurAcceleration'][$tick] = round(abs($this->myCarStats['acceleration'][$tick] - $this->myCarStats['nextAcceleration'][$tick]),12);
    }


    //maj des résultats spécifique au bot pour la course actuelle
    private function updateStats($msg)
    {
        $tick = $this->currentTick;

        //distance sur le tronçon
        $this->myCarStats['pieceLength'][$tick] = $this->currentPieceLength;

        //distance sur le tronçon
        $this->myCarStats['inPieceDistance'][$tick] = $this->myCarData['piecePosition']['inPieceDistance'];

        //distance sur le tronçon
        $this->myCarStats["pieceIndex"][$tick] = $this->currentPieceIndex;

        //drift de la voiture
        $this->myCarStats['drift'][$tick] = $this->myCarData['angle'];

        //vitesse réelle
        $this->myCarStats['speed'][$tick] = $this->computeSpeed($tick);

        //accélération
        $this->myCarStats['acceleration'][$tick] = $this->computeAcceleration($tick);

        //radius actuel
        $this->myCarStats['laneRadius'][$tick] = $this->currentLaneRadius;

        //radius actuel
        $this->myCarStats['centrifugalForce'][$tick] = $this->computeCentrifugalForce($this->myCarStats['speed'][$tick], $this->myCarStats['laneRadius'][$tick]);

        //radius actuel
        $this->myCarStats['brakingDistance'][$tick] = $this->computeBrakingDistance($this->myCarStats['speed'][$tick]);

    }


    //retourne la vitesse qui est en fait la distance parcourue entre deux tick (horloge du serveur)
    private function computeSpeed($tick)
    {
        if($tick == 0) return 0;
        $currentDistance = $this->myCarStats['inPieceDistance'][$tick];
        $prevDistance = $this->myCarStats['inPieceDistance'][$tick - 1];



        if($currentDistance != 0)
        {
            $speed = ($currentDistance > $prevDistance)
                ? $currentDistance - $prevDistance
                : $this->myCarStats['pieceLength'][$tick - 1] - $prevDistance + $currentDistance;
        }
        else
        {
            $speed =  $currentDistance;
        }

        return $speed;
    }

    //retourne l'acceleration réelle
    private function computeAcceleration($tick)
    {
        if($tick == 0) return 0;
        return $this->myCarStats['speed'][$tick] - $this->myCarStats['speed'][$tick-1];
    }


    //Calcule la force centrifuge en fonction de la vitesse et du rayon du virage
    //attention à bien fournir le radius correspondant à la voie de la voiture
    private function computeCentrifugalForce($v, $r)
    {
        return ($r != 0)? pow($v,2)/$r : 0;
    }

    //Calcule la distance de freinage en fonction de la vitesse actuelle
    private function computeBrakingDistance($v)
    {
        return pow($v,2)/(2 * $this->coeffDeceleration);
    }



    private function bendsAreOpposite($i,$j)
    {
        return(
            $this->raceData["track"]["pieces"][$i]["radius"] > 0
            && $this->raceData["track"]["pieces"][$j]["radius"] < 0
            ||
            $this->raceData["track"]["pieces"][$i]["radius"] < 0
            && $this->raceData["track"]["pieces"][$j]["radius"] > 0
        );
    }
    //-----------------------------------------------------------------------------------------------------//
    //                                         TRACK INFORMATIONS                                          //
    //-----------------------------------------------------------------------------------------------------//


    private function printTrackResults($msg)
    {
        if($this->debug)
        {
            printTitle("FIN DE LA COURSE");
            $total = $msg['data']['results'][$this->myCarPosition]['result']['millis'];
            $bestLap = $msg['data']['bestLaps'][$this->myCarPosition]['result']['millis'];

            printLn("Temps Total   : ". intval($total/1000) .".". $total%1000);
            printLn("Meileur Temps : ". intval($bestLap/1000) .".". $bestLap%1000);
        }
    }


    private function debugMsg($msg, $title = "DEBUG")
    {
        if($this->debug)
        {
            printTitle($title);
            print_r($msg);
        }
    }

}
?>

