<?php
define('MAX_LINE_LENGTH', 1024 * 1024);

require_once("Dijkstra.php");
require_once 'fonctions.php';

class BasicBot {
	protected $sock, $debug;

    private $raceData,
            $nbPieces,
            $nbLanes,
            $currentPieceIndex,
            $currentPieceLength,
            $currentLane,
            $myCarIndex,
            $myCarName,
            $myCarData,
            $myCarThrottle,
            $raceResults,
            $raceGraph,
            $bestPath;




      //-----------------------------------------------------------------------------------------------------//
     //                                                 CONSTRUCTEUR                                        //
    //-----------------------------------------------------------------------------------------------------//

    function __construct($host, $port, $botname, $botkey, $debug = FALSE)
    {
		$this->debug = $debug;  //pour débugger
        $this->myCarName = $botname; //the teamName
        $this->myCarThrottle = 1.0; //a fond les ballons ;)

        //connection
		$this->connect($host, $port, $botkey);

        $this->write_msg('join', array(
			'name' => $botname,
			'key' => $botkey
		));
	}

    function __destruct() {
        if (isset($this->sock)) {
            socket_close($this->sock);
        }
    }





      //-----------------------------------------------------------------------------------------------------//
     //                                                 CONNECTION                                          //
    //-----------------------------------------------------------------------------------------------------//

	protected function connect($host, $port, $botkey) {
		$this->sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($this->sock === FALSE) {
			throw new Exception('socket: ' . socket_strerror(socket_last_error()));
		}
		if (!socket_connect($this->sock, $host, $port)) {
			throw new Exception($host . ': ' . $this->sockerror());
		}
	}

	protected function read_msg() {
		$line = socket_read($this->sock, MAX_LINE_LENGTH, PHP_NORMAL_READ);
		if ($line === FALSE) {
			$this->debug('** ' . $this->sockerror());
		} else {
			$this->debug('<= ' . rtrim($line));
		}
		return json_decode($line, TRUE);
	}

	protected function write_msg($msgtype, $data) {
		$str = json_encode(array('msgType' => $msgtype, 'data' => $data)) . "\n";
		$this->debug('=> ' . rtrim($str));
		if (socket_write($this->sock, $str) === FALSE) {
			throw new Exception('write: ' . $this->sockerror());
		}
	}
	
	protected function sockerror() {
		return socket_strerror(socket_last_error($this->sock));
	}
	
	protected function debug($msg) {
		if ($this->debug) {
			echo $msg, "\n";
		}
	}




      //-----------------------------------------------------------------------------------------------------//
     //                                              RUN AND WIN !                                          //
    //-----------------------------------------------------------------------------------------------------//

    //receptionne l'ensemble des message envoyer par le serveur et  répond si nécessaire
    public function run()
    {
		while (!is_null($msg = $this->read_msg())) {
			switch ($msg['msgType']) {

                //conduit la voiture
				case 'carPositions' :   $this->driveMyCar($msg);
                                        break;

                //initialisation des paramètre de la course
				case 'gameInit'     :   $this->initParameters($msg);
                                        break;

                //début de la course
				case 'gameStart'    :   break;

				case 'crash'        :   $this->debugMsg($msg, "Crash");
                                        break;

                //afficher les résultats de la course
				case 'gameEnd'      :   $this->printTrackResults($msg);
                                        break;

                //ne rien faire
                case 'join'         :
                case 'yourCar'      :
                case 'spawn'        :
                case 'lapFinished'  :
                case 'dnf'          :   break;

                //envoyer un ping
				default             :   $this->write_msg('ping', null);

			}
		}
	}




      //-----------------------------------------------------------------------------------------------------//
     //                                              INITIALISATION                                         //
    //-----------------------------------------------------------------------------------------------------//

    //initialisation des paramètres de la course et du bot
    private function initParameters($msg)
    {
        $this->raceData = $msg['data']['race'];
        $this->nbPieces = count($this->raceData["track"]["pieces"]);
        $this->nbLanes = count($this->raceData["track"]["pieces"]);
        $this->raceResults = array( "maxThrottle"   => 0.0,
                                    "minThrottle"   => 1.0,
                                    "maxDrift"      => 0.0);

        //trouve l'index de la voiture en fonction du nom
        //ATTENTION à vérifier avec plusieurs concurrent si l'index ne change pendant la course
        foreach($this->raceData['cars'] as $i => $car)
        {
            if($this->myCarName == $car["id"]["name"])
            {
                $this->myCarIndex = $i;
            }
        }

        //construit le graph du circuit nécessaire pour trouver le chemin le plus court
        $this->buildRaceGraph();
        $this->bestPath = $this->findTheBestPath();
    }





      //-----------------------------------------------------------------------------------------------------//
     //                                           PEDAL ON THE METAL !!!!                                   //
    //-----------------------------------------------------------------------------------------------------//

    //envoie le message de vitesse et de changement de direction
    private function driveMyCar($msg)
    {
        //met à jour les paramètres
        $this->updateParameters($msg);


        $this->bestPath[indexName($i,$j)]

        getIndexByName


        //change de voie si nécessaire
        $this->computeSwitch();//calcule la vitesse idéale
        $this->write_msg('throttle', $this->myCarThrottle);//envoie le message de vitesse

        $this->sendSwitchLane($msg);


        $this->computeThrottle();//calcule la vitesse idéale et envoie le message de vitesse

    }

    //calcule la vitesse idéale en fonction de différent paramètres
    private function computeThrottle()
    {

        $throttle = 0.0;
        $pieceIndex = $this->currentPieceIndex;
        $nextPieceIndex = ($pieceIndex == ($this->nbPieces - 1))? 0 : $pieceIndex + 1;

        switch($this->getPieceType($pieceIndex))
        {
            case 'ligne' :
                //Freiner avant un virage ou accélerer si on est dans une ligne droite
                switch($this->getTypeOfPiece($nextPieceIndex))
                {
                    case 'ligne' :
                        $throttle = 0.84;
                        break;
                    default :
                        $throttle = 0.70;

                        break;
                }
                break;
            default  :
                $throttle = 0.6567;//vitesse minimale
                break;
        }



        $throttle = 0.6567;

        //correction en fonction du drift de la voiture

        $drift = abs($this->myCarData["angle"]);

        /*
        if($drift >= 10)
        {
            $throttle -= ($drift - 10) / 100;
        }
        else
        {
            $throttle += (10 - $drift) / 100;
        }
        */

        //la vitesse de sécurité sur Keimola est 0.6567.
        $this->myCarThrottle = max(0.6567,min(1.0,$throttle));

        $this->updateResults($throttle,$drift);//maj des résultats spécifique au bot pour la course actuelle

        $this->write_msg('throttle', $this->myCarThrottle);
    }

    //Maj des parametres du bot
    private function updateParameters($msg)
    {
        $this->myCarData = $msg['data'][$this->myCarIndex];
        $this->currentLane = $this->myCarData['piecePosition']['lane']['startLaneIndex'];

        if($followLaneIndex != $laneIndex)
        {
            $distance = $this->getSwitchLength($pieceIndex,$laneIndex, $followLaneIndex);
        }
        else
        {
            $distance = $this->getPieceLength($pieceIndex,$laneIndex);//récupère la longueur de la pièce
        }


        if($this->currentLane != $msg['data'][$this->myCarIndex]['piecePosition']['lane']['startLaneIndex'])
        {
            $this->currentPieceIndex = $pieceIndex;//maj l'index
        }

        //change l'index de la pièce courante
        $pieceIndex = $this->myCarData['piecePosition']['pieceIndex'];

        if($this->currentPieceIndex != $pieceIndex)
        {
            $this->currentPieceIndex = $pieceIndex;//maj l'index
            $this->currentPieceLength = $this->getPieceLength($this->currentPieceIndex,$this->currentLane);

        }


    }



    //envoie le message de changement de voie si nécessaire
    private function sendSwitchLane($msg)
    {
        //trouver le sens du prochain virage

        //si le prochain virage est à droite, je me place à gauche
        $side = "Left";

        //et inversement
        $side = "Right";

        //{"msgType": "switchLane", "data": "Left"}
        //$this->write_msg('switchLane', $side);
    }




      //-----------------------------------------------------------------------------------------------------//
     //                                              PIECES FUNCTIONS                                       //
    //-----------------------------------------------------------------------------------------------------//

    //récupère le type de la pièce à l'index passé en paramètres
    private function getPieceType($pieceIndex)
    {
        $piece = $this->raceData['track']['pieces'][$pieceIndex];
        $type = 'ligne';
        if($piece['angle'])
        {
            $type = 'int';
        }
        return $type;
    }


    private function getSpeed()
    {


    }

    //récupère le prochain index de piece
    private function getFollowPieceIndex($pieceIndex)
    {
        $followPieceIndex = ($pieceIndex == $this->nbPieces - 1 )? 0 : $pieceIndex + 1;
        return $followPieceIndex;
    }

    private function getPieceLength($pieceIndex,$laneIndex)
    {
        $pieceLength = 0;
        if(! $this->raceData['track']['pieces'][$pieceIndex]['length'])
        {
            $pi = 3.1415926535898;
            $angle = $this->raceData['track']['pieces'][$pieceIndex]['angle'];
            $radius = $this->raceData['track']['pieces'][$pieceIndex]['radius'];
            $laneRadius = $this->raceData['track']['lanes'][$laneIndex]['distanceFromCenter'];

            $distance = ($angle > 0)? - $laneRadius : $laneRadius ;
            $newRadius = $radius + $distance;
            $pieceLength = ($pi * abs($angle) * $newRadius) / 180;
        }
        else
        {
            $pieceLength = $this->raceData['track']['pieces'][$pieceIndex]['length'];
        }

        return $pieceLength;
    }


    private function getSwitchLength($pieceIndex,$laneIndex, $followLaneIndex)
    {
        //calcul de l'hypothènuse
        if($this->raceData['track']['pieces'][$pieceIndex]['radius'])
        {
            $angle = abs($this->raceData['track']['pieces'][$pieceIndex]['angle']);
            $radius = $this->raceData['track']['pieces'][$pieceIndex]['radius'];
            $ab = $radius + $this->raceData['track']['lanes'][$laneIndex]['distanceFromCenter'];
            $ac = $radius + $this->raceData['track']['lanes'][$followLaneIndex]['distanceFromCenter'];
        }
        else
        {
            $angle = 90;
            $ab = abs($this->raceData['track']['lanes'][$laneIndex]['distanceFromCenter'] - $this->raceData['track']['lanes'][$followLaneIndex]['distanceFromCenter']);
            $ac = $this->raceData['track']['pieces'][$pieceIndex]['length'];
        }

        $bc = sqrt( pow($ab,2) +  pow($ac,2) - (2 * $ac * $ab * cos(deg2rad($angle))));
        return $bc;
    }





      //-----------------------------------------------------------------------------------------------------//
     //                                              RACE FUNCTIONS                                         //
    //-----------------------------------------------------------------------------------------------------//


    //construit le graph de la course
    private function buildRaceGraph()
    {
        //dans un premier temps on va essayer de trouver le chemin le plus court avec l'algo de Dijkstra
        $this->raceGraph = new Graph();

        //pour chaque couple troncon/voie calculer la distance
        foreach($this->raceData['track']['pieces'] as $pieceIndex => $piece)
        {
            foreach($this->raceData['track']['lanes'] as $laneIndex => $lane)
            {
                //construire un tableau des voie accesible
                $accessibleLane = array($laneIndex);

                //si on peut changer de voie, ajouter les voie dispo
                if($piece['switch'])
                {
                    if($laneIndex > 0 )
                    {
                        $accessibleLane[] = $laneIndex - 1;
                    }

                    if($laneIndex < $this->nbLanes - 1)
                    {
                        $accessibleLane[] = $laneIndex + 1;
                    }
                }

                foreach($accessibleLane as $followLaneIndex)
                {
                    $followPieceIndex = $this->getFollowPieceIndex($pieceIndex);


                    if($followLaneIndex != $laneIndex)
                    {

                        $distance = $this->getSwitchLength($pieceIndex,$laneIndex, $followLaneIndex);
                    }
                    else
                    {
                        $distance = $this->getPieceLength($pieceIndex,$laneIndex);//récupère la longueur de la pièce
                    }

                    $from = indexName($pieceIndex,$laneIndex);
                    $to = indexName($followPieceIndex,$followLaneIndex);
                    $this->raceGraph->addedge($from, $to, $distance);

                }
            }
        }
    }

    private function findTheBestPath()
    {
        printTitle("nodes");
        $this->raceGraph->printNodes();

        list($distances, $prev) = $this->raceGraph->paths_from(indexName($this->currentPieceIndex,$this->myC));

        print_r($this->raceGraph->paths_from(indexName(0,0)));
        echo("<br>\n");
        $path = $this->raceGraph->paths_to($prev, indexName($this->nbPieces - 1,$this->currentPieceIndex));
        print_r($path);
        return $path;
    }





      //-----------------------------------------------------------------------------------------------------//
     //                                         TRACK INFORMATIONS                                          //
    //-----------------------------------------------------------------------------------------------------//


    //maj des résultats spécifique au bot pour la course actuelle
    private function updateResults($throttle, $drift)
    {
        if($throttle < $this->raceResults["minThrottle"])
        {
            $this->raceResults["minThrottle"] =  $throttle;
        }

        if($this->raceResults["maxThrottle"] < $throttle)
        {
            $this->raceResults["maxThrottle"] =  $throttle;
        }

        if($this->raceResults["maxDrift"] < $drift)
        {
            $this->raceResults["maxDrift"] =  $drift;
        }
    }


    private function printTrackResults($msg)
    {
        printTitle("FIN DE LA COURSE");
        $total = $msg['data']['results'][$this->myCarIndex]['result']['millis'];
        $bestLap = $msg['data']['bestLaps'][$this->myCarIndex]['result']['millis'];

        printLn("Temps Total   : ". intval($total/1000) .".". $total%1000);
        printLn("Meileur Temps : ". intval($bestLap/1000) .".". $bestLap%1000);
        printLn("Vitesse MAX   : ". $this->raceResults["maxThrottle"]);
        printLn("Vitesse MIN   : ". $this->raceResults["minThrottle"]);
        printLn("Drift MAX     : ". $this->raceResults["maxDrift"]);
    }


    private function debugMsg($msg, $title = "DEBUG")
    {
        if($this->debug)
        {
            printTitle($title);
            print_r($msg);

            printTitle("RESULTATS");
            print_r($this->raceResults);
            die;
        }
    }

}
?>

