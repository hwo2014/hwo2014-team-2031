<?php
define('MAX_LINE_LENGTH', 1024 * 1024);

require_once("Dijkstra.php");
require_once 'fonctions.php';

class BasicBot {
	protected $sock, $debug;

    private $raceData,
            $nbPieces,
            $nbLanes,
            $currentPieceIndex,
            $currentPieceLength,
            $myCarIndex,
            $myCarName,
            $myCarData,
            $myCarStats,
            $myCarThrottle,
            $raceGraph,
            $lanesLength,
            $bestPath,
            $waitSwitchingLane,
            $switchSide;




      //-----------------------------------------------------------------------------------------------------//
     //                                                 CONSTRUCTEUR                                        //
    //-----------------------------------------------------------------------------------------------------//

    function __construct($host, $port, $botname, $botkey, $debug = FALSE)
    {
		$this->debug = $debug;  //pour débugger
        $this->myCarName = $botname; //the teamName
        $this->myCarThrottle = 1.0; //a fond les ballons ;)
        $this->waitSwitchingLane = false;
        $this->currentPieceIndex = -1;

        $this->myCarStats = array( "pieceLength"        => array(),     //distance du tronçon
                                   "inPieceDistance"    => array(),     //distance de la voiture à l'intérieur du tronçon
                                   "throttle"           => array(),     //les gazs
                                   "speed"              => array(),     //vitesse réelle
                                   "acceleration"       => array(),     //accélération
                                   "drift"              => array());    //drift de la voiture


        //connection
		$this->connect($host, $port, $botkey);

        $this->write_msg('join', array(
			'name' => $botname,
			'key' => $botkey
		));

        /*
        $this->write_msg('joinRace', array( 'botId' => array('name' => $botname,
                                                            'key' => $botkey),
                                            'trackName' => 'germany',
                                            'carCount' => 1));

        */

	}

    function __destruct() {
        if (isset($this->sock)) {
            socket_close($this->sock);
        }
    }





      //-----------------------------------------------------------------------------------------------------//
     //                                                 CONNECTION                                          //
    //-----------------------------------------------------------------------------------------------------//

	protected function connect($host, $port, $botkey) {
		$this->sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
		if ($this->sock === FALSE) {
			throw new Exception('socket: ' . socket_strerror(socket_last_error()));
		}
		if (!socket_connect($this->sock, $host, $port)) {
			throw new Exception($host . ': ' . $this->sockerror());
		}
	}

	protected function read_msg() {
		$line = socket_read($this->sock, MAX_LINE_LENGTH, PHP_NORMAL_READ);
		if ($line === FALSE) {
			$this->debug('** ' . $this->sockerror());
		} else {
			$this->debug('<= ' . rtrim($line));
		}
		return json_decode($line, TRUE);
	}

	protected function write_msg($msgtype, $data) {
		$str = json_encode(array('msgType' => $msgtype, 'data' => $data)) . "\n";
		$this->debug('=> ' . rtrim($str));
		if (socket_write($this->sock, $str) === FALSE) {
			throw new Exception('write: ' . $this->sockerror());
		}
	}
	
	protected function sockerror() {
		return socket_strerror(socket_last_error($this->sock));
	}
	
	protected function debug($msg) {
		if ($this->debug) {
			echo $msg, "\n";
		}
	}




      //-----------------------------------------------------------------------------------------------------//
     //                                              RUN AND WIN !                                          //
    //-----------------------------------------------------------------------------------------------------//

    //receptionne l'ensemble des message envoyer par le serveur et  répond si nécessaire
    public function run()
    {
		while (!is_null($msg = $this->read_msg())) {
			switch ($msg['msgType']) {

                //conduit la voiture
				case 'carPositions' :   $this->driveMyCar($msg);
                                        break;

                //initialisation des paramètre de la course
				case 'gameInit'     :   $this->initParameters($msg);
                                        break;

                //début de la course
				case 'gameStart'    :   break;

				case 'crash'        :   $this->debugMsg($msg, "Crash");
                                        break;

                //afficher les résultats de la course
				case 'gameEnd'      :   $this->printTrackResults($msg);
                                        break;

                //ne rien faire
                case 'join'         :
                case 'yourCar'      :
                case 'spawn'        :
                case 'lapFinished'  :
                case 'dnf'          :   break;

                //envoyer un ping
				default             :   $this->write_msg('ping', null);

			}
		}
	}




      //-----------------------------------------------------------------------------------------------------//
     //                                              INITIALISATION                                         //
    //-----------------------------------------------------------------------------------------------------//

    //initialisation des paramètres de la course et du bot
    private function initParameters($msg)
    {
        $this->raceData = $msg['data']['race'];
        $this->nbPieces = count($this->raceData["track"]["pieces"]);
        $this->nbLanes = count($this->raceData["track"]["lanes"]);

        //trouve l'index de la voiture en fonction du nom
        //ATTENTION à vérifier avec plusieurs concurrent si l'index ne change pendant la course
        foreach($this->raceData['cars'] as $i => $car)
        {
            if($this->myCarName == $car["id"]["name"])
            {
                $this->myCarIndex = $i;
            }
        }

        //construit le graph du circuit nécessaire pour trouver le chemin le plus court
        $this->buildRaceGraph();
        $this->bestPath = $this->findTheBestPath();
    }





      //-----------------------------------------------------------------------------------------------------//
     //                                           PEDAL ON THE METAL !!!!                                   //
    //-----------------------------------------------------------------------------------------------------//

    //envoie le message de vitesse et de changement de direction
    private function driveMyCar($msg)
    {
        //met à jour les paramètres
        $this->updateParameters($msg);

        //change de voie si nécessaire
        $this->computeSwitch();

        //calcule la vitesse idéale et envoie le message de vitesse
        $this->computeThrottle();

        //met à jour les statistiques de la voiture et de la course
        $this->updateStats($msg);

    }

    //calcule la vitesse idéale en fonction de différent paramètres
    private function computeThrottle()
    {
        //0.6567
        $throttle = 0.5;

        if($this->currentPieceIndex >= 31 || $this->myCarData['piecePosition']['lap'] > 0)
        {
            $throttle = 1;
        }
        $throttle = 1;
        /*

        if($this->currentPieceIndex == 35)
        {
            $throttle = 0.9;
        }

        if($this->currentPieceIndex == 36   || $this->currentPieceIndex == 10)
        {
            $throttle = 0.75;
        }

        if( $this->currentPieceIndex == 11)
        {
            $throttle = 0.7;
        }
*/
        //correction en fonction du drift de la voiture
        $drift = abs($this->myCarData["angle"]);

        //la vitesse de sécurité sur Keimola est 0.6567.
        $this->myCarThrottle = max(0,min(1.0,$throttle));

        $this->write_msg('throttle', $this->myCarThrottle);
    }


    //envoie le message de changement de voie si nécessaire
    private function computeSwitch()
    {
        $followPieceIndex = $this->getFollowPieceIndex($this->currentPieceIndex);

        //si le prochain tronçon est un switch
          if($this->raceData['track']['pieces'][$this->currentPieceIndex]['switch']
           || $this->raceData['track']['pieces'][$followPieceIndex]['switch'])
        {
            //essaye de se rapprocher du la voie idéale.
            printLn("followPieceIndex ".$followPieceIndex);

            $bestLane = $this->bestPath[$this->getFollowPieceIndex($followPieceIndex)];
            $currentStartLane = $this->myCarData['piecePosition']['lane']['startLaneIndex'];
            $currentEndLane = $this->myCarData['piecePosition']['lane']['endLaneIndex'];

            //reset après changement de voie
            if($this->waitSwitchingLane && $currentStartLane != $currentEndLane)
            {
                $this->waitSwitchingLane = false;
            }
            //changer de voie
            else if(!$this->waitSwitchingLane && $bestLane !=  $currentEndLane)
            {
                $this->waitSwitchingLane = true;
                $this->switchSide = ($bestLane < $currentStartLane)? "Left" : "Right";
                $this->write_msg('switchLane', $this->switchSide);
            }
        }
    }

    //Maj des parametres du bot
    private function updateParameters($msg)
    {
        $this->myCarData = $msg['data'][$this->myCarIndex];
        $startLaneIndex = $this->myCarData['piecePosition']['lane']['startLaneIndex'];
        $endLaneIndex = $this->myCarData['piecePosition']['lane']['endLaneIndex'];

        //change l'index de la pièce courante
        $pieceIndex = $this->myCarData['piecePosition']['pieceIndex'];
        if($this->currentPieceIndex != $pieceIndex)
        {
            $this->currentPieceIndex = $pieceIndex;//maj l'index

            //récupère la longueur de la pièce
            if($startLaneIndex != $endLaneIndex) //pour un switch
            {
                $this->currentPieceLength = $this->getSwitchLength($pieceIndex,$startLaneIndex, $endLaneIndex);
            }
            else //pour un switch
            {
                $this->currentPieceLength = $this->getPieceLength($pieceIndex,$startLaneIndex);
            }
        }
    }


      //-----------------------------------------------------------------------------------------------------//
     //                                              PIECES FUNCTIONS                                       //
    //-----------------------------------------------------------------------------------------------------//

    //récupère le type de la pièce à l'index passé en paramètres
    private function getPieceType($pieceIndex)
    {
        $piece = $this->raceData['track']['pieces'][$pieceIndex];
        $type = 'ligne';
        if($piece['angle'])
        {
            $type = 'int';
        }
        return $type;
    }


    //récupère le prochain index de piece
    private function getFollowPieceIndex($pieceIndex)
    {
        $followPieceIndex = ($pieceIndex == $this->nbPieces - 1 )? 0 : $pieceIndex + 1;
        return $followPieceIndex;
    }

    //récupère l 'index précedent de piece
    private function getPreviousPieceIndex($pieceIndex)
    {
        $previousPieceIndex = ($pieceIndex == 0 )? $this->nbPieces - 1 : $pieceIndex - 1;
        return $previousPieceIndex;
    }

    private function getPieceLength($pieceIndex,$laneIndex)
    {
        $pieceLength = 0;
        if(! $this->raceData['track']['pieces'][$pieceIndex]['length'])
        {
            $pi = 3.1415926535898;
            $angle = $this->raceData['track']['pieces'][$pieceIndex]['angle'];
            $radius = $this->raceData['track']['pieces'][$pieceIndex]['radius'];
            $laneRadius = $this->raceData['track']['lanes'][$laneIndex]['distanceFromCenter'];

            $distance = ($angle > 0)? - $laneRadius : $laneRadius ;
            $newRadius = $radius + $distance;
            $pieceLength = ($pi * abs($angle) * $newRadius) / 180;
        }
        else
        {
            $pieceLength = $this->raceData['track']['pieces'][$pieceIndex]['length'];
        }

        return $pieceLength;
    }


    private function getSwitchLength($pieceIndex,$laneIndex, $followLaneIndex)
    {
        //calcul de l'hypothènuse
        if($this->raceData['track']['pieces'][$pieceIndex]['radius'])
        {
            $angle = abs($this->raceData['track']['pieces'][$pieceIndex]['angle']);
            $radius = $this->raceData['track']['pieces'][$pieceIndex]['radius'];
            $ab = $radius + $this->raceData['track']['lanes'][$laneIndex]['distanceFromCenter'];
            $ac = $radius + $this->raceData['track']['lanes'][$followLaneIndex]['distanceFromCenter'];
        }
        else
        {
            $angle = 90;
            $ab = abs($this->raceData['track']['lanes'][$laneIndex]['distanceFromCenter'] - $this->raceData['track']['lanes'][$followLaneIndex]['distanceFromCenter']);
            $ac = $this->raceData['track']['pieces'][$pieceIndex]['length'];
        }

        $bc = sqrt( pow($ab,2) +  pow($ac,2) - (2 * $ac * $ab * cos(deg2rad($angle))));
        return $bc;
    }





      //-----------------------------------------------------------------------------------------------------//
     //                                              RACE FUNCTIONS                                         //
    //-----------------------------------------------------------------------------------------------------//


    //construit le graph de la course
    private function buildRaceGraph()
    {
        //dans un premier temps on va essayer de trouver le chemin le plus court avec l'algo de Dijkstra
        $this->raceGraph = new Graph();

        //pour chaque couple troncon/voie calculer la distance
        foreach($this->raceData['track']['pieces'] as $pieceIndex => $piece)
        {
            foreach($this->raceData['track']['lanes'] as $laneIndex => $lane)
            {
                //construire un tableau des voie accesible
                $accessibleLane = array($laneIndex);

                //si on peut changer de voie, ajouter les voie dispo
                if($piece['switch'])
                {
                    if($laneIndex > 0 )
                    {
                        $accessibleLane[] = $laneIndex - 1;
                    }

                    if($laneIndex < $this->nbLanes - 1)
                    {
                        $accessibleLane[] = $laneIndex + 1;
                    }
                }

                foreach($accessibleLane as $followLaneIndex)
                {
                    $followPieceIndex = $this->getFollowPieceIndex($pieceIndex);

                    if($followLaneIndex != $laneIndex)
                    {

                        $distance = $this->getSwitchLength($pieceIndex,$laneIndex, $followLaneIndex);
                    }
                    else
                    {
                        $distance = $this->getPieceLength($pieceIndex,$laneIndex);//récupère la longueur de la pièce
                    }

                    $from = indexName($pieceIndex,$laneIndex);
                    $to = indexName($followPieceIndex,$followLaneIndex);
                    $this->raceGraph->addedge($from, $to, $distance);

                }
            }
        }
    }

    //teste pour toutes les position de départ et d'arrivée,
    //quel chemin est le plus court
    private function findTheBestPath()
    {
        $bestPath = array();
        $bestDistance = INF;

        for($indexStartLane = 0; $indexStartLane < $this->nbLanes; $indexStartLane++)
        {
            for($indexEndLane = 0; $indexEndLane < $this->nbLanes; $indexEndLane++)
            {
                list($distance, $predecessors) = $this->raceGraph->paths_from(indexName(0,$indexStartLane));
                $path = $this->raceGraph->paths_to($predecessors, indexName($this->nbPieces - 1,$indexEndLane));

                //si la distance parcourue est la meilleure alors on sauvegarde le parcours idéal
                if($distance[$path[count($path) - 1]] < $bestDistance)
                {
                    $bestPath = $path;
                }
            }
        }

        //formate le tableau pour avoir indexPiece => indexLane
        foreach($bestPath as $indexPiece => $value)
        {
            list(,$indexLane) = explode("_",$value);
            $bestPath[$indexPiece] = $indexLane;
        }

        print_r($bestPath);


        return $bestPath;
    }


      //-----------------------------------------------------------------------------------------------------//
     //                                         TRACK INFORMATIONS                                          //
    //-----------------------------------------------------------------------------------------------------//


    //maj des résultats spécifique au bot pour la course actuelle
    private function updateStats($msg)
    {
        if($msg['gameTick'])
        {

            $tick = $msg['gameTick'];

            //distance sur le tronçon
            $this->myCarStats['pieceLength'][$tick] = $this->currentPieceLength;

            //distance sur le tronçon
            $this->myCarStats['inPieceDistance'][$tick] = $this->myCarData['piecePosition']['inPieceDistance'];

            //la puissance des gazs
            $this->myCarStats['throttle'][$tick] = $this->myCarThrottle;

            //drift de la voiture
            $this->myCarStats['drift'][$tick] = abs($this->myCarData['angle']);

            //vitesse réelle
            $this->myCarStats['speed'][$tick] = ($tick > 0)? $this->computeSpeed($tick) : 0;

            //accélération
            $this->myCarStats['acceleration'][$tick] = ($tick > 0)? $this->computeAcceleration($tick) : 0;
        }
    }

    //retourne la vitesse qui est en fait la distance parcourue entre deux tick (horloge du serveur)
    private function computeSpeed($tick)
    {
        $currentDistance = $this->myCarStats['inPieceDistance'][$tick];
        $prevDistance = $this->myCarStats['inPieceDistance'][$tick - 1];
        $distance = ($currentDistance > $prevDistance)
                        ? $currentDistance - $prevDistance
                        : $this->myCarStats['pieceLength'][$tick - 1] - $prevDistance + $currentDistance;


        return $distance;
    }

    //retourne la vitesse qui est en fait la distance parcourue entre deux tick (horloge du serveur)
    private function computeAcceleration($tick)
    {
        return $this->myCarStats['speed'][$tick] - $this->myCarStats['speed'][$tick-1];
    }

    private function printStats()
    {
        printTitle('STATS');
        foreach($this->myCarStats as $name => $tab)
        {
            printLn($name . " MAX     : ". max($tab));
            printLn($name . " MIN     : ". min($tab));
        }
    }


    private function printTrackResults($msg)
    {
        printTitle("FIN DE LA COURSE");
        $total = $msg['data']['results'][$this->myCarIndex]['result']['millis'];
        $bestLap = $msg['data']['bestLaps'][$this->myCarIndex]['result']['millis'];

        printLn("Temps Total   : ". intval($total/1000) .".". $total%1000);
        printLn("Meileur Temps : ". intval($bestLap/1000) .".". $bestLap%1000);

        $this->printStats();
    }


    private function debugMsg($msg, $title = "DEBUG")
    {
        if($this->debug)
        {
            printTitle($title);
            print_r($msg);

            $this->printStats();
            die;

        }
    }

}
?>

