<?php

/*
 * Author: doug@neverfear.org
 */

require("Dijkstra.php");

function runTest() {
	$g = new Graph();
	$g->addedge("a1", "b1", 100);

	$g->addedge("a1", "b2", 99);
	$g->addedge("b2", "c1", 100);
	$g->addedge("b1", "c1", 99);


	list($distances, $prev) = $g->paths_from("a1");
	
	print_r($g->paths_from("a1"));
	echo("<br>\n");
	$path = $g->paths_to($prev, "c1");
	
	print_r($path);
	
}


runTest();

